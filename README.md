# alarm_app_testcase_bibit

A new Alarm App project for technical test of Bibit/Stockbit.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## How to Use the Apps
1. Open the Apps, and then Sign In with Credentials. You can fill with the TextField with anything according the format (Email with Email format, Password with at least 4 characters)
2. Click Login and you will see the Home Page
3. Click the time to edit the Alarm
4. Click the toggle to turn on or turn off the Alarm
5. You will get notification when the Timezone same as the Alarm that you set
6. If you click the Notification, you will get Information and turn of the Alarm