import 'package:alarm_app_testcase_bibit/common/models/models.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class Chart extends StatefulWidget {
  final List<Charts>? data;

  const Chart({
    Key? key,
    this.data,
  }) : super(key: key);

  @override
  _ChartState createState() => _ChartState();
}

class _ChartState extends State<Chart> {
  List<Chart> _barChartList = [];

  @override
  void initState() {
    _barChartList = [
      Chart(),
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<charts.Series<Charts, String>> series = [
      charts.Series(
        id: "Alarms Tap",
        data: widget.data!,
        domainFn: (Charts series, _) => series.time!,
        measureFn: (Charts series, _) =>
            double.parse(series.countAlarmRinging!),
      ),
    ];

    return Container(
      height: MediaQuery.of(context).size.height / 2.3,
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          Expanded(
            child: charts.BarChart(
              series,
              animate: true,
            ),
          ),
        ],
      ),
    );
  }
}
