import 'dart:async';

import 'package:alarm_app_testcase_bibit/ui/ui.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';

class DashboardScreen extends StatefulWidget {
  const DashboardScreen({
    Key? key,
  }) : super(key: key);

  @override
  _DashboardScreenState createState() => _DashboardScreenState();
}

class _DashboardScreenState extends State<DashboardScreen> {
  // ignore: unused_field
  String? _setTime, _timeString, _selectedTimeString;
  String? _hour, _minute, _time;

  TimeOfDay selectedTime = TimeOfDay(hour: 00, minute: 00);

  TextEditingController _timeController = TextEditingController();

  bool isSwitched = false;

  FlutterLocalNotificationsPlugin? flutterLocalNotificationsPlugin;

  @override
  void initState() {
    localNotificationInit();
    _timeString = _formatDateTime(DateTime.now());
    _timeController.text =
        formatDate(DateTime(2019, 08, 1, 00, 00), [hh, ':', nn, " ", am])
            .toString();
    Timer.periodic(Duration(seconds: 1), (Timer t) => _getTime());
    super.initState();
  }

  @override
  void dispose() {
    _timeController.dispose();
    super.dispose();
  }

  localNotificationInit() {
    /// Initialize each platform setting, basically can set the notification icon
    var initializeSettingAndroid =
        new AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializeSettingIOS = new IOSInitializationSettings();
    var initializeSetting = new InitializationSettings(
      android: initializeSettingAndroid,
      iOS: initializeSettingIOS,
    );

    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();

    flutterLocalNotificationsPlugin?.initialize(
      initializeSetting,
      onSelectNotification: onSelectNotification,
    );
  }

  void onSelectNotification(String? payload) async {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text('You open the alarm ${payload ?? '-'}'),
        duration: const Duration(seconds: 3),
      ),
    );

    setState(() {
      isSwitched = false;
    });
  }

  Future _showNotificationWithDefaultSound() async {
    AndroidNotificationDetails androidPlatformChannelSpecifics =
        new AndroidNotificationDetails(
      'alarm app BIBIT',
      'alarm app BIBIT TEST',
      channelDescription: 'Description of alarm app BIBIT TEST',
      sound: RawResourceAndroidNotificationSound('slow_spring_board'),
    );
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails();
    var platformChannelSpecifics = new NotificationDetails(
        android: androidPlatformChannelSpecifics,
        iOS: iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin!.show(
      0,
      'Wake Up!',
      'This time to you to wake up',
      platformChannelSpecifics,
      payload: _selectedTimeString,
    );
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );

    if (picked != null)
      setState(() {
        selectedTime = picked;
        _hour = selectedTime.hour.toString();
        _minute = selectedTime.minute.toString();
        _time = _hour! + ' : ' + _minute!;
        _timeController.text = _time!;
        _timeController.text = formatDate(
            DateTime(2019, 08, 1, selectedTime.hour, selectedTime.minute),
            [hh, ':', nn, " ", am]).toString();

        _selectedTimeString = "${selectedTime.hour}:${selectedTime.minute}";
        isSwitched = true;
      });

    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text('Your alarm set for $_selectedTimeString'),
        duration: const Duration(seconds: 3),
      ),
    );
  }

  void _getTime() {
    final DateTime now = DateTime.now();
    final String formattedDateTime = _formatDateTime(now);
    setState(() {
      _timeString = formattedDateTime;
    });

    if (_timeString == _selectedTimeString && isSwitched) {
      _showNotificationWithDefaultSound();
    }
  }

  String _formatDateTime(DateTime dateTime) {
    return DateFormat('HH:mm').format(dateTime);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          'Alarm App',
        ),
      ),
      body: Container(
        color: Palette.blackLighten1,
        child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 24.0,
            vertical: 20.0,
          ),
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Alarm",
                  style: FontHelper.h4Bold(
                    color: Palette.white,
                  ),
                ),
                SizedBox(
                  height: 7.0,
                ),
                Container(
                  height: 2.0,
                  color: Palette.greyLighten3,
                ),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          InkWell(
                            onTap: () {
                              _selectTime(context);
                            },
                            child: Container(
                              width: MediaQuery.of(context).size.width,
                              alignment: Alignment.centerLeft,
                              child: TextFormField(
                                style: FontHelper.custom(
                                  fontSize: 50,
                                  color: isSwitched
                                      ? Palette.white
                                      : Palette.greyLighten4,
                                ),
                                textAlign: TextAlign.start,
                                onSaved: (String? val) {
                                  _setTime = val;
                                },
                                enabled: false,
                                keyboardType: TextInputType.text,
                                controller: _timeController,
                                decoration: InputDecoration(
                                  disabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide.none,
                                  ),
                                  contentPadding: EdgeInsets.symmetric(
                                    vertical: 5.0,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(
                              horizontal: 5.0,
                            ),
                            child: Text(
                              'Alarm, wake up',
                              style: FontHelper.h6Regular(
                                color: isSwitched
                                    ? Palette.white
                                    : Palette.greyLighten4,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Switch(
                      value: isSwitched,
                      onChanged: (value) {
                        setState(() {
                          isSwitched = value;
                        });
                      },
                      activeColor: Palette.white,
                      activeTrackColor: Palette.green,
                    ),
                  ],
                ),
                SizedBox(
                  height: 17.0,
                ),
                Container(
                  height: 2.0,
                  color: Palette.greyLighten3,
                ),
                SizedBox(
                  height: 7.0 * 5,
                ),
                // Text(
                //   "Your Analytics",
                //   style: FontHelper.h4Bold(
                //     color: Palette.white,
                //   ),
                // ),
                // SizedBox(
                //   height: 7.0,
                // ),
                // Container(
                //   height: 2.0,
                //   color: Palette.greyLighten3,
                // ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
