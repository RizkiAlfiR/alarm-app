import 'dart:ui';

extension HexColorExtension on Color {
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

/// This is class for defined global color palette
class Palette {
  // Bibit Color
  static Color bibitPrimary = HexColorExtension.fromHex('#03AB6A');
  static Color bibitSecondary = HexColorExtension.fromHex('#A4E6CB');
  static Color bibitBackground = HexColorExtension.fromHex('#F0FBF5');

  // Black
  static Color black = HexColorExtension.fromHex('#202945');
  static Color blackLighten1 = HexColorExtension.fromHex('#202124');

  // White
  static Color white = HexColorExtension.fromHex('#FFFFFF');

  // Grey
  static Color grey = HexColorExtension.fromHex('#2F3C65');

  // Grey Lighten
  static Color greyLighten1 = HexColorExtension.fromHex('#989898');
  static Color greyLighten2 = HexColorExtension.fromHex('#2A2A2C');
  static Color greyLighten3 = HexColorExtension.fromHex('#303134');
  static Color greyLighten4 = HexColorExtension.fromHex('#777777');

  // Green
  static Color green = HexColorExtension.fromHex('#30C654');
  static Color greenDarken = HexColorExtension.fromHex('#005B5A');
}
