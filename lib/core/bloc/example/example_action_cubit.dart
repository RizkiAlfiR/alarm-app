// import 'dart:convert';
//
// import 'package:alarm_app_testcase_bibit/common/common.dart';
// import 'package:alarm_app_testcase_bibit/core/core.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
//
// /// This is just example action cubit
// /// Action how to save data to SharedPref
// class ExampleActionCubit extends Cubit<BaseState> {
//   final BaseLocalStorageClient localStorageClient;
//
//   ExampleActionCubit({
//     required this.localStorageClient,
//   }) : super(InitializedState());
//
//   Future<void> saveExampleData({
//     required ModelExample data,
//   }) async {
//     /// Save to local storage
//     try {
//       await localStorageClient.saveByKey(
//         jsonEncode(data.toJson()),
//         SharedPreferenceKeys.EXAMPLE_DATA,
//         SharedPrefType.STRING,
//       );
//     } catch (e) {
//       emit(
//         ErrorState(error: "Terjadi kesalahan saat menyimpan data"),
//       );
//
//       return;
//     }
//
//     emit(
//       SuccessState(
//         data: data,
//       ),
//     );
//   }
//
//   Future<void> getDataPref() async {
//     String? _rawPrefData;
//     ModelExample? _prefData;
//
//     /// Get Pref Data
//     try {
//       _rawPrefData = await localStorageClient.getByKey(
//         SharedPreferenceKeys.EXAMPLE_DATA,
//         SharedPrefType.STRING,
//       );
//     } catch (e) {
//       emit(
//         ErrorState(
//           error: '$this - Get Pref Example Data] - Error : $e',
//           timestamp: DateTime.now(),
//         ),
//       );
//       return;
//     }
//
//     /// Parse Raw Pref Data to Model
//     try {
//       if (_rawPrefData != null) {
//         _prefData = ModelExample.fromJson(jsonDecode(_rawPrefData));
//       }
//     } catch (e) {
//       emit(
//         ErrorState(
//           error: '$this - Parse Pref Example Data] - Error : $e',
//           timestamp: DateTime.now(),
//         ),
//       );
//       return;
//     }
//
//     /// All Validate Pass
//     emit(LoadedState(
//       data: _prefData,
//       timestamp: DateTime.now(),
//     ));
//   }
// }
