// import 'package:alarm_app_testcase_bibit/common/common.dart';
// import 'package:alarm_app_testcase_bibit/core/bloc/base_state.dart';
// import 'package:alarm_app_testcase_bibit/core/core.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
//
// /// Cubit that responsible for handling example dummy data
// class ExampleCubit extends Cubit<BaseState> {
//   // final AuthenticationDataCubit authenticationDataCubit;
//   // final AuthenticationActionCubit authenticationActionCubit;
//   final BaseLocalStorageClient localStorageClient;
//   final BaseExampleRepository exampleRepository;
//
//   ExampleCubit({
//     required this.localStorageClient,
//     required this.exampleRepository,
//   }) : super(InitializedState());
//
//   /// Get & Parse My Project Data from API
//   void getData() async {
//     emit(LoadingState());
//
//     List<ModelExample> _result = [];
//
//     /// Get List Example Data From Repository
//     try {
//       _result = await exampleRepository.getDummyData();
//
//       if (_result.isEmpty) {
//         emit(EmptyState());
//         return;
//       }
//     } catch (e, s) {
//       print("===> Error $e");
//       print("===> Error $s");
//       emit(
//         ErrorState(
//           error: '$this - Get List Example Data] - Error : $e',
//           timestamp: DateTime.now(),
//         ),
//       );
//       return;
//     }
//
//     /// All Validate Pass
//     emit(LoadedState(
//       data: _result,
//       timestamp: DateTime.now(),
//     ));
//   }
// }
