import 'package:alarm_app_testcase_bibit/common/models/models.dart';
import 'package:alarm_app_testcase_bibit/core/bloc/base_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AnalyticsCubit extends Cubit<BaseState> {
  AnalyticsCubit() : super(InitializedState());

  void getData() async {
    emit(LoadingState());

    Future.delayed(
      Duration(seconds: 5),
    );

    List<Charts> _result = [];

    try {
      _result.add(
        new Charts(
          time: '12:00',
          countAlarmRinging: '3',
        ),
      );
      _result.add(
        new Charts(
          time: '14:00',
          countAlarmRinging: '2',
        ),
      );
      _result.add(
        new Charts(
          time: '21:00',
          countAlarmRinging: '6',
        ),
      );
      _result.add(
        new Charts(
          time: '19:00',
          countAlarmRinging: '1',
        ),
      );
      _result.add(
        new Charts(
          time: '06:00',
          countAlarmRinging: '2',
        ),
      );
    } catch (e, s) {
      print('Error get Data e: $e');
      print('Error get Data s: $s');
      emit(
        ErrorState(
          error: 'error $e',
          timestamp: DateTime.now(),
        ),
      );
      return;
    }

    LoadedState(
      data: _result,
      timestamp: DateTime.now(),
    );
  }
}
