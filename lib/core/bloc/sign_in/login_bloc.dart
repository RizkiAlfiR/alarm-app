import 'dart:async';

import 'package:alarm_app_testcase_bibit/common/models/models.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:formz/formz.dart';

part 'login_event.dart';
part 'login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginState());

  @override
  Stream<LoginState> mapEventToState(
    LoginEvent event,
  ) async* {
    if (event is EmailChanged) {
      yield _mapEmailChangedToState(event, state);
    } else if (event is PasswordChanged) {
      yield _mapPasswordChangedToState(event, state);
    } else if (event is Submitted) {
      yield* _mapSubmittedToState(event, state);
    }
  }

  LoginState _mapEmailChangedToState(EmailChanged event, LoginState state) {
    final email = Email.dirty(event.email);

    return state.copyWith(
        email: email, status: Formz.validate([state.password!, email]));
  }

  LoginState _mapPasswordChangedToState(
      PasswordChanged event, LoginState state) {
    final password = Password.dirty(event.password);

    return state.copyWith(
        password: password, status: Formz.validate([password, state.email!]));
  }

  Stream<LoginState> _mapSubmittedToState(
      Submitted event, LoginState state) async* {
    ///TODO: Call API POST
  }
}
