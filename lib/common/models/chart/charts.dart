class Charts {
  final String? time;
  final String? countAlarmRinging;

  Charts({
    this.time,
    this.countAlarmRinging,
  });
}
